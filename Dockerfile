# https://hub.docker.com/_/ubuntu/tags
FROM ubuntu:noble-20250127@sha256:72297848456d5d37d1262630108ab308d3e9ec7ed1c3286a32fe09856619a782

ENV DEBIAN_FRONTEND noninteractive
# Do not cache apt packages
# https://wiki.ubuntu.com/ReducingDiskFootprint
RUN echo 'Acquire::http {No-Cache=True;};' > /etc/apt/apt.conf.d/no-cache && \
    echo 'APT::Install-Recommends "0"; APT::Install-Suggests "0";' > /etc/apt/apt.conf.d/01norecommend && \
    echo 'Dir::Cache { srcpkgcache ""; pkgcache ""; }' > /etc/apt/apt.conf.d/02nocache && \
    echo 'Acquire::GzipIndexes "true"; Acquire::CompressionTypes::Order:: "gz";' > /etc/apt/apt.conf.d/02compress-indexes

SHELL ["/bin/bash", "-c"]

RUN apt -y update && \
    # Software installation (for curl, add-apt-repository, apt-key)
    apt -y --no-install-recommends install ca-certificates curl dirmngr git gpg gpg-agent wget unzip zip software-properties-common build-essential make gcc g++ sudo cron dos2unix tini && \
    curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg && \
    echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list && \
    apt -y update && \
    apt -y --no-install-recommends install \
        # Process managers
        supervisor \
        # install net packages useful for debugging
        iputils-ping telnet netcat-openbsd lsof net-tools openssl dnsutils rsync bind9-host stress pv \
        # config file manipulation
        crudini xmlstarlet moreutils jq \
        # General purpose
        pwgen swaks vim nano cmake pkg-config openssh-client uuid less zip file \
        # apache
        apache2 libapache2-mod-perl2 apache2-dev \
        # nginx
        nginx-full \
        # image optimizers
        optipng pngquant jpegoptim \
        # Databases (clients)
        sqlite3 mysql-client-8.0 redis-tools postgresql-client-16 ldap-utils \
        # Dev packages (useful for native modules in ruby, node)
        gettext imagemagick graphicsmagick libcurl4 libcurl4-openssl-dev libexpat1-dev libffi-dev libgdbm-dev libicu-dev libmysqlclient-dev \
        libncurses5-dev libpq-dev libre2-dev libreadline-dev libssl-dev libxml2-dev libxslt-dev libyaml-dev zlib1g-dev \
        libmcrypt-dev libgmp-dev libfreetype6-dev libjpeg-dev libjpeg-turbo8-dev libpng-dev chrpath libxft-dev libfontconfig1-dev \
        libkrb5-dev libxslt1-dev libldap2-dev libsasl2-dev libtool libvips libzmq3-dev locales-all locales libmagic1 xmlsec1 \
        # perl
        perl libimage-exiftool-perl \
        # Python 3
        python3-dev python3-pip python3-setuptools python3-venv \
        # php 8.3 (mailparse is broken - https://bugs.launchpad.net/ubuntu/+source/php-mailparse/+bug/2063334)
        php8.3 php8.3-{bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,gmp,imap,interbase,intl,ldap,mbstring,mysql,odbc,opcache,pgsql,phpdbg,pspell,readline,snmp,soap,sqlite3,sybase,tidy,xml,xmlrpc,xsl,zip} libapache2-mod-php8.3 php-{apcu,date,fpdf,imagick,gnupg,pear,redis,smbclient,twig,uuid,validate,zmq} composer \
        # good to have!
        ghostscript libgs-dev ffmpeg x264 x265 && \
    # Delete apt-cache and let people apt-update on start. Without this, we keep getting apt errors for --fix-missing
    rm -rf /var/cache/apt /var/lib/apt/lists

## node . we don't use the nodejs/npm package because the npm deb brings in each npm module as deb package!
ARG NODE_VERSION=22.14.0
RUN mkdir -p /usr/local/node-$NODE_VERSION && \
    curl -L https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-$NODE_VERSION
ENV PATH /usr/local/node-$NODE_VERSION/bin:$PATH

# mongo shell (https://www.mongodb.com/try/download/shell) and mongo tools (mongorestore, mongodump)
# https://www.mongodb.com/download-center/database-tools/releases/archive
RUN curl https://downloads.mongodb.com/compass/mongodb-mongosh_2.4.0_amd64.deb -o /tmp/mongosh.deb && \
    dpkg -i /tmp/mongosh.deb && \
    rm /tmp/mongosh.deb && \
    curl https://fastdl.mongodb.org/tools/db/mongodb-database-tools-ubuntu2204-x86_64-100.11.0.deb -o /tmp/mongotools.deb && \
    dpkg -i /tmp/mongotools.deb && \
    rm /tmp/mongotools.deb

# gosu (https://github.com/tianon/gosu/releases)
RUN curl -L https://github.com/tianon/gosu/releases/download/1.17/gosu-amd64 -o /usr/local/bin/gosu && chmod +x /usr/local/bin/gosu

# https://github.com/mikefarah/yq/releases
ARG YQVERSION=4.45.1
RUN curl -sL https://github.com/mikefarah/yq/releases/download/v${YQVERSION}/yq_linux_amd64 -o /usr/bin/yq && chmod +x /usr/bin/yq

# add a non-previleged user that apps can use
# by default, account is created as inactive which prevents login via openssh
# https://github.com/gitlabhq/gitlabhq/issues/5304
RUN userdel --remove ubuntu && \
    adduser --uid 1000 --disabled-login --gecos 'Cloudron' cloudron && \
    passwd -d cloudron

# add the two commonly used users to the volume group
RUN addgroup --gid 500 --system media && \
    usermod -a -G media cloudron && \
    usermod -a -G media www-data

# disable editor features which don't work with read-only fs
RUN echo "set noswapfile" >> /root/.vimrc && \
    gosu cloudron:cloudron bash -c 'echo "set noswapfile" >> /home/cloudron/.vimrc' && \
    echo "unset historylog" >> /etc/nanorc

# this also sets /etc/default/locale (see detailed notes in README)
RUN update-locale LANG=en_US.UTF-8 LC_CTYPE=en_US.UTF-8 LC_ALL=en_US.UTF-8

# create symlinks for the dotdirs
RUN for dir in .cache .config .node-gyp .npm .mongodb .ssh .yarn .local; do \
        rm -rf /root/${dir} && ln -sf /run/root${dir} /root/${dir} && mkdir /run/root${dir}; \
        rm -rf /home/cloudron/${dir} && ln -sf /run/cloudron${dir} /home/cloudron/${dir} && mkdir /run/cloudron${dir} && chown --no-dereference cloudron:cloudron /home/cloudron/${dir}; chown cloudron:cloudron /run/cloudron${dir}; \
    done

# create symlinks for the dotfiles
RUN for file in .bash_history .dbshell .inputrc .irb_history .mongorc.js .mysql_history .psql_history .mongoshrc.js .yarnrc; do \
        ln -sf /run/root${file} /root/${file}; \
        ln -sf /run/cloudron${file} /home/cloudron/${file} && chown --no-dereference cloudron:cloudron /home/cloudron/${file}; \
    done

COPY cloudron_addons /usr/local/cloudron_addons

# source any app specific rcfile
RUN echo -e "\nsource /usr/local/cloudron_addons\n\n[[ -f /app/data/.bashrc ]] && source /app/data/.bashrc\n" >> /root/.bashrc

