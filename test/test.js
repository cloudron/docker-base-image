'use strict';

const execSync = require('child_process').execSync,
    expect = require('expect.js');

describe('Base image test', function () {
    this.timeout(0);

    it('check nodejs', function () {
        const result = execSync('docker run cloudron/basetest node --version', { encoding: 'utf8' });
        expect(result.trim()).to.be('v18.18.0');
    });

    it('check mysql', function () {
        const result = execSync('docker run cloudron/basetest mysql --version', { encoding: 'utf8' });
        expect(result.trim()).to.contain('Ver 8.0');
    });

    it('check PHP', function () {
        const result = execSync('docker run cloudron/basetest php --version', { encoding: 'utf8' });
        expect(result.trim()).to.contain('PHP 8.1');
    });

    it('check mongo', function () {
        let result = execSync('docker run cloudron/basetest mongosh --version', { encoding: 'utf8' });
        expect(result.trim()).to.contain('1.10');

        result = execSync('docker run cloudron/basetest mongorestore --version', { encoding: 'utf8' });
        expect(result.trim()).to.contain('100.8');
    });

    it('check gosu', function () {
        const result = execSync('docker run cloudron/basetest gosu --version', { encoding: 'utf8' });
        expect(result.trim()).to.contain('1.16');
    });

    it('check yq', function () {
        const result = execSync('docker run cloudron/basetest yq --version', { encoding: 'utf8' });
        expect(result.trim()).to.contain('4.35');
    });

});

